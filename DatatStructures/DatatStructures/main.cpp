﻿// DatatStructures.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "queue.h"

int main()
{
    queue* q = new queue;
    initQueue(q, 5);
    std::cout << "Is empty: " << (bool)isEmpty(q) << std::endl;
    enqueue(q, 1);
    enqueue(q, 2);
    enqueue(q, 3);
    enqueue(q, 4);
    enqueue(q, 5);
    std::cout << "Is full: " << (bool)isFull(q) << std::endl;
    std::cout << "Dequeued item: " << dequeue(q) << std::endl;
}
